import { createCookieSessionStorage, redirect } from "@remix-run/node";
import bcrypt from "bcryptjs";
import prisma from "~/utils/prisma";

const sessionStorage = createCookieSessionStorage({
  cookie: {
    name: "user_session",
    sameSite: "lax",
    path: "/",
    httpOnly: true,
    secrets: [process.env.SESSION_SECRET ?? "session_secret"],
    secure: process.env.NODE_ENV === "production",
  },
});

function getSession(request: Request) {
  const cookie = request.headers.get("Cookie");
  return sessionStorage.getSession(cookie);
}

async function createSession(request: Request, userId: String) {
  const session = await getSession(request);
  session.set("userId", userId);
  return redirect("/dashboard", {
    headers: {
      "Set-Cookie": await sessionStorage.commitSession(session, {
        maxAge: 60 * 60 * 24 * 1,
      }),
    },
  });
}

export async function destroySession(request: Request) {
  const session = await getSession(request);
  return redirect("/sign-in", {
    headers: {
      "Set-Cookie": await sessionStorage.destroySession(session),
    },
  });
}

export async function signIn(
  request: Request,
  email: string,
  password: string
) {
  const user = await prisma.user.findUnique({ where: { email } });
  if (!user) {
    throw new Error("invalid email");
  }
  if (!(await bcrypt.compare(password, user.password))) {
    throw new Error("invalid password");
  }
  return createSession(request, user.id);
}

export async function signUp(name: string, email: string, password: string) {
  const hash = await bcrypt.hash(password, 10);
  await prisma.user.create({ data: { name, email, password: hash } });
  return redirect("/sign-in");
}

export async function getUserId(request: Request): Promise<string> {
  const session = await getSession(request);
  const userId = session.get("userId");
  return userId;
}
