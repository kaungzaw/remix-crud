import { useState } from "react";
import { Layout, Menu } from "antd";
import { useLocation, Link } from "@remix-run/react";

const { Content, Sider } = Layout;

export default function MainLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const location = useLocation();
  const [collapsed, setCollapsed] = useState(false);

  return (
    <Layout hasSider>
      <Sider
        style={{
          overflow: "auto",
          height: "100vh",
          position: "fixed",
          left: 0,
          top: 0,
          bottom: 0,
        }}
        theme="dark"
        breakpoint="lg"
        collapsedWidth="0"
        onCollapse={(collapsed) => {
          setCollapsed(collapsed);
        }}
      >
        <Menu
          theme="dark"
          mode="inline"
          selectedKeys={[location.pathname.split("/")[1]]}
          items={[
            {
              key: "dashboard",
              label: <Link to="/dashboard">Dashboard</Link>,
            },
            {
              key: "item",
              label: <Link to="/item">Item</Link>,
            },
            {
              key: "category",
              label: <Link to="/category">Category</Link>,
            },
            {
              key: "setting",
              label: <Link to="/setting">Setting</Link>,
            },
            {
              key: "sign-out",
              label: <Link to="/sign-out">Sign Out</Link>,
            },
          ]}
        />
      </Sider>
      <Layout
        style={{
          marginLeft: collapsed ? 0 : 200,
        }}
      >
        <Menu
          style={{
            overflow: "auto",
            position: "fixed",
            zIndex: 10,
          }}
          hidden={!collapsed}
          theme="dark"
          mode="inline"
          triggerSubMenuAction="click"
          selectedKeys={[location.pathname.split("/")[1]]}
          items={[
            {
              key: "nav",
              label: "Nav",
              children: [
                {
                  key: "dashboard",
                  label: <Link to="/dashboard">Dashboard</Link>,
                },
                {
                  key: "category",
                  label: <Link to="/category">Category</Link>,
                },
                {
                  key: "item",
                  label: <Link to="/item">Item</Link>,
                },
                {
                  key: "setting",
                  label: <Link to="/setting">Setting</Link>,
                },
              ],
            },
          ]}
        />
        <Content
          style={{
            marginTop: collapsed ? 50 : 0,
          }}
        >
          <div
            style={{
              padding: 15,
              minHeight: "100vh",
              background: "#fff",
            }}
          >
            {children}
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
