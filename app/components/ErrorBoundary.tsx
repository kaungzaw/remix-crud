import { Modal, Result, Button } from "antd";
import { useLocation, Link } from "@remix-run/react";

export default function ErrorBoundary() {
  const location = useLocation();
  Modal.destroyAll();

  return (
    <Result
      status="error"
      title="Something went wrong."
      extra={
        <Link to={location.pathname}>
          <Button>Reload</Button>
        </Link>
      }
    />
  );
}
