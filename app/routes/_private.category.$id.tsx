import { Suspense } from "react";
import { Button, Form, Input, Skeleton } from "antd";
import { Link, Await, useLoaderData } from "@remix-run/react";
import { json, defer } from "@remix-run/node";
import prisma from "~/utils/prisma";
import useFetcherWithPromise from "~/utils/useFetcherWithPromise";

import type { ActionFunctionArgs, LoaderFunctionArgs } from "@remix-run/node";
import type { Category } from "@prisma/client";

export async function action({ request }: ActionFunctionArgs) {
  const formData = await request.formData();
  const id = formData.get("id")?.toString();
  const name = formData.get("name")?.toString();
  if (!(id && name)) {
    throw new Error();
  }
  await prisma.category.update({
    where: {
      id,
    },
    data: {
      name,
    },
  });
  return json({ ok: true });
}

export async function loader({ params }: LoaderFunctionArgs) {
  const category = new Promise<Category>((resolve) => {
    prisma.category
      .findUniqueOrThrow({ where: { id: params.id } })
      .then((data) => resolve(data));
  });
  return defer({ category });
}

export { default as ErrorBoundary } from "~/components/ErrorBoundary";

export default function UpdateCategory() {
  const [form] = Form.useForm();
  const { category } = useLoaderData<typeof loader>();
  const fetcher = useFetcherWithPromise();

  return (
    <Suspense fallback={<Skeleton active />}>
      <Await resolve={category}>
        {(category) => (
          <>
            <Link to="/category">
              <Button>Back</Button>
            </Link>
            <br />
            <br />
            <Form
              form={form}
              initialValues={category}
              layout="vertical"
              disabled={fetcher.state !== "idle"}
              onFinish={async (values) => {
                if (form.isFieldsTouched()) {
                  await fetcher.submit(
                    { id: category.id, name: values.name },
                    { method: "post" }
                  );
                  form.resetFields();
                }
              }}
            >
              <Form.Item
                label="Name"
                name="name"
                rules={[{ required: true, message: "Name is required" }]}
              >
                <Input name="name" />
              </Form.Item>
              <Form.Item>
                <Button htmlType="submit" loading={fetcher.state !== "idle"}>
                  Submit
                </Button>
                &nbsp;&nbsp;&nbsp;
                <Button htmlType="reset" danger>
                  Reset
                </Button>
              </Form.Item>
            </Form>
          </>
        )}
      </Await>
    </Suspense>
  );
}
