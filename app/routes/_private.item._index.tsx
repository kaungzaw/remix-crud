import { Suspense } from "react";
import { Table, Button, Modal, Skeleton } from "antd";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { json, defer } from "@remix-run/node";
import { Link, Await, useLoaderData } from "@remix-run/react";
import prisma from "~/utils/prisma";
import useFetcherWithPromise from "~/utils/useFetcherWithPromise";

import type { Item } from "@prisma/client";
import type { ColumnsType } from "antd/lib/table";
import type { ActionFunctionArgs } from "@remix-run/node";

export async function action({ request }: ActionFunctionArgs) {
  const formData = await request.formData();
  const id = formData.get("id")?.toString();
  if (!id) {
    throw new Error();
  }
  await prisma.item.delete({
    where: {
      id,
    },
  });
  return json({ ok: true });
}

export function loader() {
  const item = new Promise<Array<Item>>((resolve) => {
    prisma.item
      .findMany({
        include: {
          category: true,
        },
      })
      .then((data) => resolve(data));
  });
  return defer({ item });
}

export { default as ErrorBoundary } from "~/components/ErrorBoundary";

export default function Item() {
  const { item } = useLoaderData<typeof loader>();
  const fetcher = useFetcherWithPromise();

  interface DataType {
    id: string;
  }

  const columns: ColumnsType<DataType> = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Category",
      dataIndex: ["category", "name"],
      key: "category",
    },
    {
      title: "Action",
      key: "action",
      render: (_, record) => {
        return (
          <>
            <Button
              onClick={() => {
                Modal.confirm({
                  title: "Are you sure?",
                  icon: <ExclamationCircleOutlined />,
                  okText: "Yes",
                  okType: "danger",
                  cancelText: "No",
                  onOk: () =>
                    fetcher.submit({ id: record.id }, { method: "post" }),
                  onCancel: () => {},
                });
              }}
              size="small"
              danger
            >
              Delete
            </Button>
            &nbsp;&nbsp;&nbsp;
            <Link to={`/item/${record.id}`}>
              <Button size="small">Edit</Button>
            </Link>
          </>
        );
      },
    },
  ];

  return (
    <Suspense fallback={<Skeleton active />}>
      <Await resolve={item}>
        {(item) => (
          <>
            <Link to="/item/create">
              <Button>Create</Button>
            </Link>
            <br />
            <br />
            <Table
              columns={columns}
              dataSource={item}
              rowKey="id"
              size="middle"
              bordered
            />
          </>
        )}
      </Await>
    </Suspense>
  );
}
