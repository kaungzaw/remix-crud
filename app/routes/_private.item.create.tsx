import { Suspense } from "react";
import { Button, Form, Input, Select, Skeleton } from "antd";
import { Link, Await, useLoaderData, useFetcher } from "@remix-run/react";
import { redirect, defer } from "@remix-run/node";
import prisma from "~/utils/prisma";

import type { Category } from "@prisma/client";
import type { ActionFunctionArgs } from "@remix-run/node";

export async function action({ request }: ActionFunctionArgs) {
  const formData = await request.formData();
  const name = formData.get("name")?.toString();
  const categoryId = formData.get("categoryId")?.toString();
  if (!(name && categoryId)) {
    throw new Error();
  }
  await prisma.item.create({
    data: {
      name,
      categoryId,
    },
  });
  return redirect("/item");
}

export const loader = async () => {
  const category = new Promise<Array<Category>>((resolve) => {
    prisma.category.findMany().then((data) => resolve(data));
  });
  return defer({ category });
};

export { default as ErrorBoundary } from "~/components/ErrorBoundary";

export default function CreateItem() {
  const [form] = Form.useForm();
  const fetcher = useFetcher();
  const { category } = useLoaderData<typeof loader>();

  return (
    <Suspense fallback={<Skeleton active />}>
      <Await resolve={category}>
        {(category) => (
          <>
            <Link to="/item">
              <Button>Back</Button>
            </Link>
            <br />
            <br />
            <Form
              form={form}
              layout="vertical"
              disabled={fetcher.state !== "idle"}
              onFinish={(values) => fetcher.submit(values, { method: "post" })}
            >
              <Form.Item
                label="Name"
                name="name"
                rules={[{ required: true, message: "Name is required" }]}
              >
                <Input name="name" />
              </Form.Item>
              <Form.Item
                label="Category"
                name="categoryId"
                rules={[{ required: true, message: "Category is required" }]}
              >
                <Select
                  options={category.map((i) => ({
                    label: i.name,
                    value: i.id,
                  }))}
                />
              </Form.Item>

              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={fetcher.state !== "idle"}
                >
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </>
        )}
      </Await>
    </Suspense>
  );
}
