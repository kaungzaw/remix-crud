import { Suspense } from "react";
import { Button, Form, Input, Select, Skeleton } from "antd";
import { Link, Await, useLoaderData } from "@remix-run/react";
import { json, defer } from "@remix-run/node";
import prisma from "~/utils/prisma";
import useFetcherWithPromise from "~/utils/useFetcherWithPromise";

import type { ActionFunctionArgs, LoaderFunctionArgs } from "@remix-run/node";
import type { Category } from "@prisma/client";

export async function action({ request }: ActionFunctionArgs) {
  const formData = await request.formData();
  const id = formData.get("id")?.toString();
  const name = formData.get("name")?.toString();
  const categoryId = formData.get("categoryId")?.toString();
  if (!(id && name && categoryId)) {
    throw new Error();
  }
  await prisma.item.update({
    where: {
      id,
    },
    data: {
      name,
      categoryId,
    },
  });
  return json({ ok: true });
}

export async function loader({ params }: LoaderFunctionArgs) {
  const item = new Promise<Category>((resolve) => {
    prisma.item
      .findUniqueOrThrow({
        where: { id: params.id },
        include: { category: true },
      })
      .then((data) => resolve(data));
  });
  const category = new Promise<Array<Category>>((resolve) => {
    prisma.category.findMany().then((data) => resolve(data));
  });
  return defer({ data: Promise.all([item, category]) });
}

export { default as ErrorBoundary } from "~/components/ErrorBoundary";

export default function UpdateCategory() {
  const [form] = Form.useForm();
  const { data } = useLoaderData<typeof loader>();
  const fetcher = useFetcherWithPromise();

  return (
    <Suspense fallback={<Skeleton active />}>
      <Await resolve={data}>
        {([item, category]) => (
          <>
            <Link to="/item">
              <Button>Back</Button>
            </Link>
            <br />
            <br />
            <Form
              form={form}
              initialValues={item}
              layout="vertical"
              disabled={fetcher.state !== "idle"}
              onFinish={async (values) => {
                if (form.isFieldsTouched()) {
                  await fetcher.submit(
                    { id: item.id, ...values },
                    { method: "post" }
                  );
                  form.resetFields();
                }
              }}
            >
              <Form.Item
                label="Name"
                name="name"
                rules={[{ required: true, message: "Name is required" }]}
              >
                <Input name="name" />
              </Form.Item>
              <Form.Item
                label="Category"
                name="categoryId"
                rules={[{ required: true, message: "Category is required" }]}
              >
                <Select
                  options={category.map((i) => ({
                    label: i.name,
                    value: i.id,
                  }))}
                />
              </Form.Item>
              <Form.Item>
                <Button htmlType="submit" loading={fetcher.state !== "idle"}>
                  Submit
                </Button>
                &nbsp;&nbsp;&nbsp;
                <Button htmlType="reset" danger>
                  Reset
                </Button>
              </Form.Item>
            </Form>
          </>
        )}
      </Await>
    </Suspense>
  );
}
