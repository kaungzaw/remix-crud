import { Outlet } from "@remix-run/react";
import { redirect } from "@remix-run/node";
import { getUserId } from "~/utils/auth";
import MainLayout from "~/components/MainLayout";

import type { LoaderFunctionArgs } from "@remix-run/node";

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const userId = await getUserId(request);
  if (!userId) {
    return redirect("/sign-in");
  }
  return null;
};

export default function Private() {
  return (
    <MainLayout>
      <Outlet />
    </MainLayout>
  );
}
