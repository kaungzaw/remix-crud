import { Form, Input, Button } from "antd";
import { useFetcher } from "@remix-run/react";
import { signUp } from "~/utils/auth";

import type { ActionFunctionArgs } from "@remix-run/node";

const regex = new RegExp("^(?=.*[0-9])(?=.*[a-z])(?!.* ).{8,}$");

export async function action({ request }: ActionFunctionArgs) {
  const formData = await request.formData();
  const name = formData.get("name")?.toString();
  const email = formData.get("email")?.toString();
  const password = formData.get("password")?.toString();
  if (!(name && email && password)) {
    throw new Error();
  }
  return await signUp(name, email, password);
}

export default function SignUp() {
  const fetcher = useFetcher();

  return (
    <div
      style={{
        minWidth: "300px",
        width: "40vw",
        margin: "100px auto 0px auto",
        textAlign: "right",
      }}
    >
      <Form
        layout="vertical"
        autoComplete="off"
        requiredMark={true}
        disabled={fetcher.state !== "idle"}
        onFinish={(values) => fetcher.submit(values, { method: "post" })}
      >
        <Form.Item
          label="Name"
          name="name"
          rules={[{ required: true, message: "Name is required!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Email"
          name="email"
          rules={[{ required: true, message: "Email is required!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Password"
          name="password"
          hasFeedback
          rules={[
            { required: true, message: "Password is required" },
            () => ({
              validator(_, value: string) {
                if (regex.test(value)) {
                  return Promise.resolve();
                }
                return Promise.reject(
                  new Error(
                    "Minimum 8 characters, at least 1 letter and 1 number"
                  )
                );
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label="Confirm Password"
          name="confirm_password"
          hasFeedback
          rules={[
            { required: true, message: "Confirm Password is required" },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }
                return Promise.reject(new Error("Passwords do not match"));
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>
        <br />
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            loading={fetcher.state !== "idle"}
          >
            Sign Up
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
