import { Form, Input, Button, message } from "antd";
import { Link } from "@remix-run/react";
import { signIn } from "~/utils/auth";

import type { ActionFunctionArgs } from "@remix-run/node";
import useFetcherWithPromise from "~/utils/useFetcherWithPromise";

export async function action({ request }: ActionFunctionArgs) {
  const formData = await request.formData();
  const email = formData.get("email")?.toString();
  const password = formData.get("password")?.toString();
  if (!(email && password)) {
    return null;
  }
  try {
    return await signIn(request, email, password);
  } catch {
    return "error";
  }
}

export default function SignIn() {
  const fetcher = useFetcherWithPromise();

  return (
    <div
      style={{
        minWidth: "300px",
        width: "40vw",
        margin: "100px auto 0px auto",
        textAlign: "right",
      }}
    >
      <Form
        layout="vertical"
        autoComplete="off"
        disabled={fetcher.state !== "idle"}
        onFinish={async (values) => {
          const data = await fetcher.submit(values, { method: "post" });
          if (data === "error") {
            message.error("Invalid Email or Password");
            fetcher.submit({}, { action: "/reset-fetcher", method: "post" });
          }
        }}
      >
        <Form.Item
          label="Email"
          name="email"
          rules={[{ required: true, message: "Email is required!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: "Password is required" }]}
        >
          <Input.Password />
        </Form.Item>
        <br />
        <Form.Item>
          <Link to="/sign-up">
            <Button>Sign Up</Button>
          </Link>
          &nbsp;&nbsp;&nbsp;
          <Button
            type="primary"
            htmlType="submit"
            loading={fetcher.state !== "idle"}
          >
            Sign In
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
