import { Outlet } from "@remix-run/react";
import { redirect } from "@remix-run/node";
import { getUserId } from "~/utils/auth";

import type { LoaderFunctionArgs } from "@remix-run/node";

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const userId = await getUserId(request);
  if (userId) {
    return redirect("/dashboard");
  }
  return null;
};

export default function Auth() {
  return <Outlet />;
}
