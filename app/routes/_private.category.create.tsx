import { Button, Form, Input } from "antd";
import { Link, useFetcher } from "@remix-run/react";
import { redirect } from "@remix-run/node";
import prisma from "~/utils/prisma";

import type { ActionFunctionArgs } from "@remix-run/node";

export async function action({ request }: ActionFunctionArgs) {
  const formData = await request.formData();
  const name = formData.get("name")?.toString();
  if (!name) {
    throw new Error();
  }
  await prisma.category.create({
    data: {
      name,
    },
  });
  return redirect("/category");
}

export { default as ErrorBoundary } from "~/components/ErrorBoundary";

export default function CreateCategory() {
  const [form] = Form.useForm();
  const fetcher = useFetcher();

  return (
    <>
      <Link to="/category">
        <Button>Back</Button>
      </Link>
      <br />
      <br />
      <Form
        form={form}
        layout="vertical"
        disabled={fetcher.state !== "idle"}
        onFinish={(values) => fetcher.submit(values, { method: "post" })}
      >
        <Form.Item
          label="Name"
          name="name"
          rules={[{ required: true, message: "Name is required" }]}
        >
          <Input name="name" />
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            loading={fetcher.state !== "idle"}
          >
            Submit
          </Button>
        </Form.Item>
      </Form>
    </>
  );
}
