import { Suspense } from "react";
import { Table, Button, Modal, Skeleton } from "antd";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { json, defer } from "@remix-run/node";
import { Link, Await, useLoaderData } from "@remix-run/react";
import prisma from "~/utils/prisma";
import useFetcherWithPromise from "~/utils/useFetcherWithPromise";

import type { Category } from "@prisma/client";
import type { ColumnsType } from "antd/lib/table";
import type { ActionFunctionArgs } from "@remix-run/node";

export async function action({ request }: ActionFunctionArgs) {
  const formData = await request.formData();
  const id = formData.get("id")?.toString();
  if (!id) {
    throw new Error();
  }
  await prisma.category.delete({
    where: {
      id,
    },
  });
  return json({ ok: true });
}

export const loader = async () => {
  const category = new Promise<Array<Category>>((resolve) => {
    prisma.category.findMany().then((data) => resolve(data));
  });
  return defer({ category });
};

export { default as ErrorBoundary } from "~/components/ErrorBoundary";

export default function Category() {
  const { category } = useLoaderData<typeof loader>();
  const fetcher = useFetcherWithPromise();

  interface DataType {
    id: string;
  }

  const columns: ColumnsType<DataType> = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Action",
      key: "action",
      render: (_, record) => {
        return (
          <>
            <Button
              onClick={() => {
                Modal.confirm({
                  title: "Are you sure?",
                  icon: <ExclamationCircleOutlined />,
                  okText: "Yes",
                  okType: "danger",
                  cancelText: "No",
                  onOk: () =>
                    fetcher.submit({ id: record.id }, { method: "post" }),
                  onCancel: () => {},
                });
              }}
              size="small"
              danger
            >
              Delete
            </Button>
            &nbsp;&nbsp;&nbsp;
            <Link to={`/category/${record.id}`}>
              <Button size="small">Edit</Button>
            </Link>
          </>
        );
      },
    },
  ];

  return (
    <Suspense fallback={<Skeleton active />}>
      <Await resolve={category}>
        {(category) => (
          <>
            <Link to="/category/create">
              <Button>Create</Button>
            </Link>
            <br />
            <br />
            <Table
              columns={columns}
              dataSource={category}
              rowKey="id"
              size="middle"
              bordered
            />
          </>
        )}
      </Await>
    </Suspense>
  );
}
